<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Album;

class AlbumController extends Controller
{
    public function index()
    {
        return view('albums.index', [
            'albums' => Auth::user()->albums
        ]);
    }

    public function save(Request $r)
    {
        if ($r->id) {
            $album = Album::find($r->id);
        } else {
            $album = new Album;
        }
        $album->name = (string) $r->name;
        $album->user_id = Auth::id();
        $album->private = (bool) $r->private;
        $album->save();

        return redirect('/albums');
    }

    public function edit($id)
    {
        $album = Album::find($id);
        if ($album && $album->user_id == Auth::id()) {
            return view('albums.edit', [
                'album' => $album
            ]);
        }
        return redirect('/albums');
    }
}
