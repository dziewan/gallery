@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card mb-4">
        <div class="card-header">Utwórz nowy album</div>
        <div class="card-body">
          <form action="{{ url('albums/save/') }}" method="post">
            @csrf
            <div class="form-group">
              <label for="name">Nazwa albumu</label>
              <input type="text" class="form-control" id="name" name="name" placeholder="Podaj nazwę albumu">
              <small class="form-text text-muted">Nazwa albumu powinna by super</small>
            </div>
            <div class="form-check mb-2">
              <input type="checkbox" class="form-check-input" id="private" name="private">
              <label class="form-check-label" for="private">Jest to album prywatny</label>
            </div>
            <button type="submit" class="btn btn-success">Dodaj nowy album</button>
          </form>
        </div>
      </div>
      <div class="card">
        <div class="card-header">Twoje albumy</div>
        <div class="card-body">
          <table class="table table-hover">
            <tr>
              <th>ID albumu</th>
              <th>Nazwa</th>
              <th>Prywatny</th>
              <th>Data utworzenia</th>
              <th>Data modyfikacji</th>
              <th>Akcje</th>
            </tr>
            @foreach ($albums as $album)
            <tr>
              <td>{{ $album->id }}</td>
              <td>{{ $album->name }}</td>
              <td>{{ $album->private?'Tak':'Nie' }}</td>
              <td>{{ $album->created_at }}</td>
              <td>{{ $album->updated_at }}</td>
              <td>
                <a href="{{ url('/albums/edit/' . $album->id) }}" class="btn btn-primary btn-sm">Edytuj</a>
              </td>
            </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
