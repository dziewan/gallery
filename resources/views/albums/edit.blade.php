@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card mb-4">
        <div class="card-header">Edycja albumu <b>{{ $album->name }}</b> (ID #{{ $album->id }})</div>
        <div class="card-body">
          <form action="{{ url('albums/save/') }}" method="post">
            @csrf
            <input type="hidden" name="id" value="{{ $album->id }}">
            <div class="form-group">
              <label for="name">Nazwa albumu</label>
              <input type="text" class="form-control" id="name" name="name" placeholder="Podaj nazwę albumu" value="{{ $album->name }}">
            </div>
            <div class="form-check mb-2">
              <input type="checkbox" class="form-check-input" id="private" name="private" {{ $album->private?'checked':'' }}>
              <label class="form-check-label" for="private">Jest to album prywatny</label>
            </div>
            <button type="submit" class="btn btn-success">Zapisz album</button>
            <a href="{{ url('albums/delete/' . $album->id) }}"
              class="btn btn-danger float-right"
              onclick="return confirm('Czy na pewno chcesz usunąć album wraz z wszystkimi zdjęciami?');">
              Usuń album
            </a>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
